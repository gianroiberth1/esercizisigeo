const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const expressSession = require('express-session')

const port = 4000
const username = "getbackup14"
const password = "getbackup14"
const dbName = "Boh"

const app = express()

app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extender: true}))
app.use(expressSession({
    secret: "cookiecookie",
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: 'auto',
        maxAge: 3600000
    }
}))

app.set('view engine', 'ejs')

//Connessione al DB 
mongoose.connect(`mongodb+srv://${username}:${password}@cluster0.pxjvn.mongodb.net/${dbName}?retryWrites=true&w=majority`, () => {
    console.log("Sono connesso al DB Mongo!")
})
//Avvio server Express
app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

global.loggato = null
global.ruolo = null

app.use("*", (req, res, next) => {
    loggato = req.session.userId
    ruolo = req.session.role
    next()
})
//Rotte
const homeController = require('./controllers/homeController')
const adminNewServiceController = require('./controllers/adminNewServiceController')
const registerPageController = require('./controllers/registerPageController')
const loginController = require('./controllers/loginController')
const registerActionController = require('./controllers/registerActionController')
const loginActionController = require('./controllers/loginActionController')
const logOutController = require('./controllers/logOutController')
const checkAdminMiddleWare = require('./middleware/checkAdminMiddleWare')
const checkUserMiddleWare = require('./middleware/checkUserMiddleWare')
const adminNewServiceSaveController = require('./controllers/adminNewServiceSaveController')
const adminViewUserController = require('./controllers/adminViewUserController')
const userStoricoOrdiniController = require('./controllers/userStoricoOrdiniController')
const adminViewOrderController = require('./controllers/adminViewOrderController')
const adminDeleteServiceController = require('./controllers/adminDeleteServiceController')
const adminDeleteUserController = require('./controllers/adminDeleteUserController')
const adminEditServiceController = require('./controllers/adminEditServiceController')
const adminEditServiceSaveController = require('./controllers/adminEditServiceSaveController')
const orderController = require('./controllers/orderController')
const orderSaveController = require('./controllers/orderSaveController')
const redirectIfNotLogged = require('./middleware/redirectIfNotLogged')

app.get("/", homeController)
app.get("/auth/register", registerPageController)
app.get("/auth/login", loginController)
app.get("/auth/logout", logOutController)
app.get("/order/:id", redirectIfNotLogged, orderController)
app.post("/order/save/:id", orderSaveController)
app.get("/user/storicoOrdini/", redirectIfNotLogged, checkUserMiddleWare, userStoricoOrdiniController)
app.get("/admin/newservice/new", redirectIfNotLogged, checkAdminMiddleWare, adminNewServiceController)
app.get("/admin/vieworder", redirectIfNotLogged, checkAdminMiddleWare, adminViewOrderController)
app.get("/admin/viewuser", redirectIfNotLogged, checkAdminMiddleWare, adminViewUserController)

app.post("/auth/register", registerActionController)
app.post("/auth/login", loginActionController)
app.post("/admin/newservice/save", redirectIfNotLogged, checkAdminMiddleWare, adminNewServiceSaveController)
app.get("/admin/viewuser/delete/:id", redirectIfNotLogged, checkAdminMiddleWare, adminDeleteUserController)
app.get("/admin/edit/:id", redirectIfNotLogged, checkAdminMiddleWare, adminEditServiceController)
app.post("/admin/edit/save/:id", redirectIfNotLogged, checkAdminMiddleWare, adminEditServiceSaveController)
app.get("/admin/delete/:id", redirectIfNotLogged, checkAdminMiddleWare, adminDeleteServiceController)


app.use((req,res) => {
    res.render('pageNotFound')
})