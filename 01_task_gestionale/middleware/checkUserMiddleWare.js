module.exports = (req, res, next) => {
    if(req.session.userId && req.session.role == "Cliente"){
        next();
    }
    else{
        res.render('notAuthorizedPage')
    }
}