module.exports = (req, res, next) => {
    if(req.session.userId && req.session.role == "Admin"){
        next();
    }
    else{
        res.render('notAuthorizedPage')
    }
}