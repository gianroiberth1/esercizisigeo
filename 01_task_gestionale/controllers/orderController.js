module.exports = (req,res) => {

    let idServizio = req.params.id

    res.render('order', {
        servizioSelezionato: idServizio
    })
}