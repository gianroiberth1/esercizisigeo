const User = require('../models/User')

module.exports = (req,res) => {
    
    let utente = {
        username: req.body.inputEmail,
        password: req.body.inputPassword,
        role: req.body.inputSelect,
    }

    try {
        User.create(utente, (err, userCreated) => {
            if(!err && userCreated){
                res.redirect("/auth/login")
            }else{
                res.render('error');
            }
        })
    } catch (error) {
        res.render('error')
    }
    
}