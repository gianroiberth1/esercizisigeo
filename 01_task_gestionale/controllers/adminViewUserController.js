const User = require("../models/User")

module.exports = async (req,res) => {
    try {
        let elenco = await User.find({})
        res.render('viewuser', {
            elencouser: elenco
        })
    } catch (error) {
        res.render('error')
    }
    
}