const User = require('../models/User')

module.exports = async (req,res) => {
        try {
                await User.findOneAndDelete({_id: req.params.id})
                res.redirect("/admin/viewuser")        
        } catch (error) {
                res.render('error')
        }
        
}