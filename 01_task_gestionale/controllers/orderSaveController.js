const Order = require('../models/Order')
const ServizioPost = require('../models/ServizioPost')

module.exports = async (req,res) => {
    
    let idUtente = req.session.userId
    let idServizio = req.params.id

    let service = await ServizioPost.findOne({"_id" : idServizio})
    let ordine = {
        user: idUtente,
        servizio: service.nome,
        pagamento: req.body.inputPagamento,
        note: req.body.inputNote,
        durata: service.durataServizio
    }

    try {
        let ordineCreato = await Order.create(ordine)
        ordineCreato
        res.render('ordersuccess')
    } catch (error) {
        res.render('error')
    }
}