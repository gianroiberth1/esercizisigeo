const ServizioPost = require("../models/ServizioPost")

module.exports = async (req,res) => {
    try {
        await ServizioPost.findOneAndDelete({ _id: req.params.id})
        res.redirect('/')
    } catch (error) {
        res.render('error')
    }
    
}