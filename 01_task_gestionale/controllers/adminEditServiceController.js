const ServizioPost = require("../models/ServizioPost")

module.exports = async (req,res) => {
    try {
        const edit = await ServizioPost.findOne({ _id: req.params.id})
        res.render('updateService', {
            modificaServizio: edit
        })
    } catch (error) {
        res.render('error')
    }
}