const Order = require("../models/Order")

module.exports = async (req,res) => {

    let orderlist = await Order.find({})

    res.render('viewOrder', {
        lista: orderlist
    })
}