const Order = require("../models/Order")
const ServizioPost = require("../models/ServizioPost")

module.exports = async (req,res) => {
    try {
        let ordine = await Order.findOne({
            "user": req.session.userId
        })
        
        console.log
        if(ordine)
            res.render('storicoOrdini', {
                myorder: ordine
            })
        else
            res.render('error')
    } catch (error) {
        res.render('error')
    }
    
}