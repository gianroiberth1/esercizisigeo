const ServizioPost = require("../models/ServizioPost")

module.exports = async (req,res) => {
    
    let aggiorna = {
        nome: req.body.inputTitle2,
        descrizione: req.body.inputDescription2,
        prezzo: req.body.inputPrice2,
        durataServizio: req.body.inputDurata2
    }
    /*
    await ServizioPost.findOneAndDelete({ _id: req.params.id})
    await ServizioPost.create(update, (err,ep) => {
        if(!err){
            res.redirect("/")
        }else{
            res.render('error')
        }
    })
    */
    try {
        await ServizioPost.findByIdAndUpdate(req.params.id, aggiorna)
    res.redirect('/')
    } catch (error) {
        res.render('error')        
    }
    
}