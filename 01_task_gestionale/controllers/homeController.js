const ServizioPost = require("../models/ServizioPost")

module.exports = async (req, res) => { // dichiaro arrow function asyncrona
    try {
        let elenco = await ServizioPost.find({})
        res.render('index', {
            elencoservizi: elenco})
    } catch (error) {
        res.render('error')
    }
    
}