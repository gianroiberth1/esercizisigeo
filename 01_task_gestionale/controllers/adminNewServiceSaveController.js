const ServizioPost = require('../models/ServizioPost')

module.exports = (req,res) => {

    let nuovoServizio = {
        nome: req.body.inputTitle,
        descrizione: req.body.inputDescription,
        prezzo: req.body.inputPrice,
        durataServizio: req.body.inputDurata
    }

    ServizioPost.create(nuovoServizio, (err,sp) =>{
        try {
            if(!err){
                res.redirect("/")
            }else{
                res.render('error')
            }
        } catch (error) {
            res.render('error')
        }
        
    })
}