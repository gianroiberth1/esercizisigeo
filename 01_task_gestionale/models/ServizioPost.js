const mongoose = require('mongoose')
const Schema = mongoose.Schema


const ServizioPostSchema = new Schema(
    {
        nome: {
            type: String,
            required: true,
            unique: true
        },
        descrizione: {
            type: String,
            required: true
        },
        prezzo: {
            type: Number,
            required: true
        },
        durataServizio: {
            type: Number,
            required: true
        }
    }
)

const ServizioPost = mongoose.model('ServizioPost', ServizioPostSchema)
module.exports = ServizioPost