const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')

const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    }
})

UserSchema.pre('save', function(next){
    let user = this
    
    user.username = user.username.toUpperCase()
    bcrypt.hash(user.password, 10, (err,passwordHashed) =>{
        user.password = passwordHashed
        next()
    })
})
const User = mongoose.model('User', UserSchema)
module.exports = User