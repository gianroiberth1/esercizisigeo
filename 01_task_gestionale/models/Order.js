const mongoose = require('mongoose')
const Schema = mongoose.Schema

const OrderSchema = new Schema(
    {
        user: String,
        servizio: String,
        data: {
            type: String,
            default: new Date()
        },
        pagamento: String,
        note: String,
        durata: Number
    }
)

const Order = mongoose.model('Order', OrderSchema)
module.exports = Order