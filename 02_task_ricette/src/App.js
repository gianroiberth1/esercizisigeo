import logo from './logo.svg';
import './App.css';
import LibroRicetteComponent from './components/Ricettario/LibroRicetteComponent';

function App() {
  return (
    <div className="container  mt-5">
      <h1 className="text-center">Libro Ricette</h1>
      <hr/>
      <LibroRicetteComponent/>
    </div>
  );
}

export default App;
