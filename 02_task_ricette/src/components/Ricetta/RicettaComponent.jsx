import React, { Component } from 'react'
import ProdottoComponent from '../Ingrediente/ProdottoComponent'

export class RicettaComponent extends Component {

    constructor(props) {
        super(props)
        
        console.log(props)
    }
    
    
    render() {

        const {nome, ing} = this.props

        return (
            <React.Fragment>
                <div class="card-body">
                        <h5 class="card-title">{nome}</h5>
                {ing.map((obj, idx) => (
                        <ProdottoComponent prodotto={obj.prodotto} descrizione={obj.descrizione} quantita={obj.quantita} um={obj.um}/>
                ))}
                </div>
                <hr/>              
            </React.Fragment>
        )
    }
}

export default RicettaComponent
