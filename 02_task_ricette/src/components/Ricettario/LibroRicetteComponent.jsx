import React, { Component } from 'react'
import RicettaComponent from '../Ricetta/RicettaComponent'

export class LibroRicetteComponent extends Component {
    constructor(props){
        super(props)
        this.libroricette = [
            {
                nome: "Pancake",
                ingredienti: [
                    {
                        prodotto: "Burro",
                        descrizione: "Burro vegano",
                        quantita: 25,
                        um: "g"
                    },
                    {
                        prodotto: "Lievito",
                        descrizione: "In polvere per dolci",
                        quantita: 6,
                        um: "g"
                    },
                    {
                        prodotto: "Uova",
                        descrizione: "Uova di dimensione media",
                        quantita: 2,
                        um: "pz"
                    },
                    {
                        prodotto: "Farina",
                        descrizione: "Di tipo 00",
                        quantita: 125,
                        um: "g"
                    },
                    {
                        prodotto: "Latte fresco",
                        descrizione: "Latte intero o di soia",
                        quantita: 200,
                        um: "g"
                    },
                    {
                        prodotto: "Zucchero",
                        descrizione: "A tua scelta",
                        quantita: 15,
                        um: "g"
                    },
                ]
            },
            {
                nome: "Carbonara",
                ingredienti: [
                    {
                        prodotto: "Spaghetti",
                        descrizione: "Solo spaghetti lunghi e corposi",
                        quantita: 320,
                        um: "g"
                    },
                    {
                        prodotto: "Guanciale",
                        descrizione: "No alla pancetta o qualsiasi altra cosa",
                        quantita: 150,
                        um: "g"
                    },
                    {
                        prodotto: "Tuorli",
                        descrizione: "Tuorli di uova medie",
                        quantita: 6,
                        um: "pz"
                    },
                    {
                        prodotto: "Pecorino romano",
                        descrizione: "Non il parmigiano",
                        quantita: 50,
                        um: "g"
                    },
                    {
                        prodotto: "Pepe",
                        descrizione: "Pepe nero",
                        quantita: 1,
                        um: "g"
                    },
                ]
            },
            {
                nome: "Polpette",
                ingredienti: [
                    {
                        prodotto: "Manzo",
                        descrizione: "Manzo macinato",
                        quantita: 500,
                        um: "g"
                    },
                    {
                        prodotto: "Uova",
                        descrizione: "Uova medie",
                        quantita: 2,
                        um: "pz"
                    },
                    {
                        prodotto: "Pepe",
                        descrizione: "Pepe nero",
                        quantita: 3,
                        um: "g"
                    },
                    {
                        prodotto: "Provola",
                        descrizione: "Provola Affumicata",
                        quantita: 125,
                        um: "g"
                    },
                    {
                        prodotto: "Prezzemolo",
                        descrizione: "Prezzemolo tritato",
                        quantita: 1,
                        um: "ciuffo"
                    },
                    {
                        prodotto: "Parmigiano",
                        descrizione: "Reggiano DOP da grattuggiare",
                        quantita: 50,
                        um: "g"
                    },
                ]
            },
        ]
    }
    
    render() {
        return (
            <React.Fragment>
                {this.libroricette.map((ricetta, idx) => (
                    <React.Fragment>
                        <RicettaComponent nome={ricetta.nome} ing={ricetta.ingredienti}/>
                    </React.Fragment>    
                ))}
            </React.Fragment>
        )
    }
}

export default LibroRicetteComponent
