import React, { Component } from 'react'

export class ProdottoComponent extends Component {
    constructor(props) {
        super(props)
    
        console.log(props)
    }
    
    render() {

        const {prodotto, descrizione, quantita, um} = this.props
        return (
            <React.Fragment>
                <ul class="list-group list-group-flush">
                        <li class="list-group-item">{prodotto} - {descrizione} - {quantita} {um}</li>
                    </ul>
                
            </React.Fragment>
        )
    }
}

export default ProdottoComponent
