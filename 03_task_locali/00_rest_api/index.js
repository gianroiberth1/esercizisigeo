const express = require('express')
const bodyParser = require('body-parser')
const pool = require('./dbConfig')
const cors = require('cors')
const app = express()

app.use(bodyParser.json())
app.use(cors())
app.use(bodyParser.urlencoded({extended: true}))

const port = 4000

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

app.get("/locale/list", async (req,res) => {
    try {
        let elenco = await pool.query('SELECT * FROM locali')
        /* console.log(`Numero di righe: ${elenco.rowCount}`) */
        res.json(elenco.rows)
    } catch (error) {
        console.log(error)
    }
})

app.get("/admin/locale/:idLocale", async (req,res) => {
    try {
        let locale= await pool.query(`SELECT * FROM locali WHERE id = ${req.params.idLocale}`)
        res.json(locale.rows)
    } catch (error) {
        console.log(error)
    }
})

app.post("/admin/locale/insert", async (req, res) => {
    try {
        let querySql = `INSERT INTO locali (nome, descrizione, citta, provincia, regione, latitudine, longitudine, voto) ` + 
            `VALUES ('${req.body.nome}', '${req.body.descrizione}', '${req.body.citta}','${req.body.provincia}', '${req.body.regione}', ${req.body.latitudine},${req.body.longitudine}, ${req.body.voto})`;

        let risultatoInserimento = await pool.query(querySql)
        
        if(risultatoInserimento.rowCount > 0){
            res.json({status: "success"})
        }
        else{
            res.json({status: "error"})
        }
    } catch (error) {
        console.log(error)
        res.json(error)
    }
})

app.delete("/admin/locale/:idLocale", async (req, res) => {
    try {
        let esitoOperazione = await pool.query(`DELETE FROM locali WHERE id = ${req.params.idLocale}`)

        if(esitoOperazione.rowCount > 0){
            res.json({status: "eliminato con successo!"})
        }
        else{
            res.json({status: "errore, dato non trovato"})
        }
    } catch (error) {
        console.log(error)
    }
})

app.put("/admin/locale/:idLocale", async (req, res) => {
    try {
        let oggettoPreSalvataggio = await pool.query(`SELECT * FROM locali WHERE id = ${req.params.idLocale}`)
        localePreSalvataggio = oggettoPreSalvataggio.rows[0]

        if(req.body.nome)
            localePreSalvataggio.nome = req.body.nome
        if(req.body.descrizione)
            localePreSalvataggio.descrizione = req.body.descrizione
        if(req.body.citta)
            localePreSalvataggio.citta = req.body.citta
        if(req.body.provincia)
            localePreSalvataggio.provincia = req.body.provincia
        if(req.body.regione)
            localePreSalvataggio.regione = req.body.regione
        if(req.body.latitudine)
            localePreSalvataggio.latitudine = req.body.latitudine
        if(req.body.longitudine)
            localePreSalvataggio.longitudine = req.body.longitudine
        if(req.body.voto)
            localePreSalvataggio.voto = req.body.voto        
         
        let queryUpdate = `UPDATE locali SET ` +
                            `nome = '${localePreSalvataggio.nome}', ` + 
                            `descrizione = '${localePreSalvataggio.descrizione}', ` + 
                            `citta = '${localePreSalvataggio.citta}', ` + 
                            `provincia = '${localePreSalvataggio.provincia}', ` + 
                            `regione = '${localePreSalvataggio.regione}', ` + 
                            `latitudine = ${localePreSalvataggio.latitudine}, ` + 
                            `longitudine = ${localePreSalvataggio.longitudine}, ` + 
                            `voto = ${localePreSalvataggio.voto} WHERE id = ${req.params.idLocale}`

        let risultatoUpdate = await pool.query(queryUpdate)

        if(risultatoUpdate.rowCount > 0){
            res.json({status: "modificato con successo!"})
        }
        else{
            res.json({status: "errore, modifica non effettuata"})
        }
    } catch (error) {
        console.log(error)
    }
})

 